package ie.ais.solr.fulltext.search.model;

import org.apache.solr.client.solrj.beans.Field;

import java.util.List;


public class Item {

    @Field("id")
    private String id;

    @Field
//    private java.util.ArrayList description;
    private List<String> description;

    @Field
    private List<String>  category;

    @Field("price")
    private float price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String>  category) {
        this.category = category;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

}
