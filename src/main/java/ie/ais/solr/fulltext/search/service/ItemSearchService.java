package ie.ais.solr.fulltext.search.service;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrServerException;

import ie.ais.solr.fulltext.search.model.Item;

public interface ItemSearchService {

    public void index(String id, List<String> description, List<String> category, float price) throws SolrServerException, IOException;

    public void indexBean(Item item) throws IOException, SolrServerException;

}
